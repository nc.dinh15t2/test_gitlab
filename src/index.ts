'use strict';
import express, { Application, Request, Response, NextFunction } from 'express';
import { Server } from 'http';
import { AddressInfo } from 'net';
import { SimpleBaseController } from './controllers/base.controller';
import user from './controllers/user.controller';

class ApiRouter extends SimpleBaseController {
  init(): void {
    this.router.get('/', (req: Request, res: Response) => {
      res.json({ message: 'Hello World' });
    });

    this.router.use('/users', user());
  }
}

const app: Application = express();

app.use('/api/v1', new ApiRouter().router);

const server: Server = app.listen(8000, 'localhost', () => {
  const { address, port } = server.address() as AddressInfo;
  console.log(`listening at ${address}:${port}`);
});
