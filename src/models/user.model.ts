import { JSONSchema, Model } from 'objection';
import knex from '../connections/model.connection';
import { v4 } from 'uuid';

export interface User {
  readonly id: string;
  username: string;
  encrypted: string;
  name: {
    first: string,
    last: string
  }
  readonly created: number;
}

export class User extends Model {
  static tableName = 'users';
  static idColumn = 'id';

  static jsonSchema: JSONSchema = {
    type: 'object',
    required: ['id','username','encrypted','created'],
    properties: {
      id: {
        type: 'string',
        readOnly: true,
        default: v4()
      },
      username: {
        type: 'string',
        minLength: 5,
        maxLength: 255
      },
      name: {
        type: 'object',
        properties: {
          first: { type: 'string' },
          last: { type: 'string' }
        }
      },
      encrypted: {
        type: 'string'
      },
      created: {
        type: 'number',
        readOnly: true,
        default: new Date().getTime()
      }
    }
  }
}

User.knex(knex);