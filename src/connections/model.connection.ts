import Knex from 'knex';

export default Knex({
  client: 'mysql',
  useNullAsDefault: true,
  connection: {
    host: 'localhost',
    port: 8006,
    user: 'dinh',
    password: '123',
    database: 'auth_db'
  },
  debug: true
});