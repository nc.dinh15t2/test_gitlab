import { NextFunction, Request, Response, Router } from 'express';
import { SimpleBaseController } from './base.controller';
import * as UserService from '../services/user.service'; 

export class UserController extends SimpleBaseController {
  init(): void {
    this.router.get('/', async (req: Request, res: Response, next: NextFunction) => {
      const users = await UserService.getAll();
      res.json(users);
    });
  }
}

export default function(): Router {
  return new UserController().router;
}