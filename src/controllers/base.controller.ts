import { Router } from "express";

export interface IController {
  router: Router;
}

export abstract class SimpleBaseController implements IController {
  router: Router;
  constructor() {
    this.router = Router({caseSensitive: true});
    this.init();
  }
  
  abstract init(): void;
}