import { User } from '../models/user.model';

export async function getAll(): Promise<User[]> {
  const users: User[] = await User.query().select('*');
  return users;
}